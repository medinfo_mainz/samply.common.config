# Samply Common Config

This library offers utilities to find and deserialize configuration files
depending on the operating system. It also offers configuration objects that
have been created from XSD files with `xjc`.

# Repository moved!

As of May 31st, 2017, the repository has moved to https://bitbucket.org/medicalinformatics/samply.common.config. Please see the following help article on how to change the repository location in your working copies:

* https://help.github.com/articles/changing-a-remote-s-url/

If you have forked Samply.common.config in Bitbucket, the fork is now linked to the new location automatically. Still, you should change the location in your local copies (usually, the origin of the fork is configured as a remote with name "upstream" when cloning from Bitbucket).